package com.icebreakerlabs.viajabessa.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by igorfeiden on 13/10/2017.
 */

public class TravelPackage implements Serializable {

    @SerializedName("package_id")
    private Long pkgId;

    @SerializedName("default_price")
    private double defaultPrice;
    private String title;

    @SerializedName("big_image")
    private String bigImageURL;

    @SerializedName("small_image")
    private String smallImageURL;

    @SerializedName("buying_prefix")
    private String buyingPrefix;


    @SerializedName("description")
    private String pkgDescription;

    private List<PriceVariation> variations = new ArrayList<>();
    private List<String> gallery = new ArrayList<>();

    public TravelPackage(Long pkgId, double defaultPrice, String title, String bigImageURL, String smallImageURL, String buyingPrefix, String pkgDescription, List<PriceVariation> variations, List<String> gallery) {
        this.pkgId = pkgId;
        this.defaultPrice = defaultPrice;
        this.title = title;
        this.bigImageURL = bigImageURL;
        this.buyingPrefix = buyingPrefix;
        this.smallImageURL = smallImageURL;
        this.pkgDescription = pkgDescription;
        this.variations = variations;
        this.gallery = gallery;
    }

    public String getBuyingPrefix() {
        return buyingPrefix;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public Long getPkgId() {
        return pkgId;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public String getTitle() {
        return title;
    }

    public String getBigImageURL() {
        return bigImageURL;
    }

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public String getPkgDescription() {
        return pkgDescription;
    }

    public List<PriceVariation> getVariations() {
        return variations;
    }

    public static class PriceVariation implements Serializable {
        private double price;
        private String perks;

        public PriceVariation(double price, String perks) {
            this.price = price;
            this.perks = perks;
        }

        public double getPrice() {
            return price;
        }

        public String getPerks() {
            return perks;
        }
    }
}
