package com.icebreakerlabs.viajabessa.network;

import com.icebreakerlabs.viajabessa.model.TravelPackage;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by igorfeiden on 16/10/2017.
 */

public interface RetrofitService {
    String ENDPOINT = "http://demo6391057.mockable.io";

    @GET("/packages")
    Call<List<TravelPackage>> getPackages();

    class ServiceGenerator {

        public static <T> T createService(Class<T> aClass) {
            return new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient())
                    .build()
                    .create(aClass);
        }
    }

}
