package com.icebreakerlabs.viajabessa;

import android.content.Context;
import android.util.DisplayMetrics;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by igorfeiden on 15/10/2017.
 */

public class Utils {

    /**
     * Receives an DP value as input, returns that dp value in pixels
     * @param dp desired dp output
     * @param context context
     * @return value in px
     *
     * Thanks, StackOverflow
     */
    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String getFormattedPrice(double price) {
        return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(price);
    }
}
