package com.icebreakerlabs.viajabessa.listeners;

import android.view.View;

/**
 * Created by igorfeiden on 15/10/2017.
 * Created because I needed two onclickListeners in two different adapters. This helped to avoid boilerplate.
 */

public interface GenericClickListener {

    void childClicked(View view, int position);

}
