package com.icebreakerlabs.viajabessa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.icebreakerlabs.viajabessa.R;
import com.icebreakerlabs.viajabessa.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by igorfeiden on 15/10/2017.
 */

public class ContentActivityGalleryAdapter extends RecyclerView.Adapter<ContentActivityGalleryAdapter.ContentGalleryVH> {

    // Andorid objs
    private LayoutInflater inflater;

    // Project objs
    private List<String> urls;

    public ContentActivityGalleryAdapter(Context context, List<String> urls) {
        this.urls = urls;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ContentGalleryVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.rv_gallery_item_layout, parent, false);
        return new ContentGalleryVH(view);
    }

    @Override
    public void onBindViewHolder(ContentGalleryVH holder, int position) {
        ImageView view = holder.view;
        Context context = view.getContext();
        Picasso.with(context)
                .load(urls.get(position))
                .resize(Utils.dpToPx(220, context), Utils.dpToPx(240, context))
                .centerCrop()
                .into(view);
    }


    @Override
    public int getItemCount() {
        return urls.size();
    }

    class ContentGalleryVH extends RecyclerView.ViewHolder {

        ImageView view;

        ContentGalleryVH(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.gallery_item_img);

        }
    }
}
