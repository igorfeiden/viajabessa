package com.icebreakerlabs.viajabessa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.icebreakerlabs.viajabessa.R;
import com.icebreakerlabs.viajabessa.Utils;
import com.icebreakerlabs.viajabessa.listeners.GenericClickListener;
import com.icebreakerlabs.viajabessa.model.TravelPackage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igorfeiden on 13/10/2017.
 */

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder> {

    // Android objs
    private LayoutInflater lInflater;

    //Project objs
    private GenericClickListener listener;
    private List<TravelPackage> tPackages = new ArrayList<>();

    public HomeRecyclerViewAdapter(List<TravelPackage> tPackages, Context context) {
        this.tPackages = tPackages;
        lInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = lInflater.inflate(R.layout.rv_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TravelPackage travelPackage = tPackages.get(position);

        holder.title.setText(travelPackage.getTitle());
        holder.price.setText(Utils.getFormattedPrice(travelPackage.getDefaultPrice()));

        Picasso.with(holder.thumbnail.getContext())
                .load(travelPackage.getSmallImageURL())
                .resize(120, 100)
                .into(holder.thumbnail);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public TravelPackage getPackageAt(int position) {
        return tPackages.size() > position ?
                tPackages.get(position) :
                null;
    }

    @Override
    public int getItemCount() {
        return tPackages.size();
    }

    public void setupListener(GenericClickListener listener) {
        this.listener = listener;
    }

    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private TextView price;
        private ImageView thumbnail;
        private RelativeLayout parent;

        private ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.content_title_tv);
            price = itemView.findViewById(R.id.content_price_tv);
            thumbnail = itemView.findViewById(R.id.content_thumbnail);

            parent = itemView.findViewById(R.id.content_parent);
            parent.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) listener.childClicked(view, getAdapterPosition());
        }
    }

}
