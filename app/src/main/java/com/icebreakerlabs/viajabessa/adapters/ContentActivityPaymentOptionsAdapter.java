package com.icebreakerlabs.viajabessa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icebreakerlabs.viajabessa.R;
import com.icebreakerlabs.viajabessa.Utils;
import com.icebreakerlabs.viajabessa.listeners.GenericClickListener;
import com.icebreakerlabs.viajabessa.model.TravelPackage;

import java.util.List;

/**
 * Created by igorfeiden on 16/10/2017.
 */

public class ContentActivityPaymentOptionsAdapter extends RecyclerView.Adapter<ContentActivityPaymentOptionsAdapter.ContentActivityPaymentVH> {

    // Project objs
    private List<TravelPackage.PriceVariation> prices;
    private GenericClickListener listener;

    // Android objs
    private final LayoutInflater inflater;

    public ContentActivityPaymentOptionsAdapter(List<TravelPackage.PriceVariation> prices, Context context) {
        this.prices = prices;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ContentActivityPaymentVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.rv_prices_item_layout, parent, false);
        return new ContentActivityPaymentVH(view);
    }

    @Override
    public void onBindViewHolder(ContentActivityPaymentVH holder, int position) {
        TravelPackage.PriceVariation priceVariation = prices.get(position);

        holder.description.setText(priceVariation.getPerks());
        holder.value.setText(Utils.getFormattedPrice(priceVariation.getPrice()));

    }

    @Override
    public int getItemCount() {
        return prices.size();
    }

    public TravelPackage.PriceVariation getPriceAt(int position) {
        return prices.size() > position ?
                prices.get(position) :
                new TravelPackage.PriceVariation(0L, "");
    }

    public void setupListener(GenericClickListener listener) {
        this.listener = listener;
    }

    class ContentActivityPaymentVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView description;
        TextView value;

        ContentActivityPaymentVH(View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.price_item_description);
            value = itemView.findViewById(R.id.price_item_value);

            itemView.findViewById(R.id.prices_parent_cardview).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) listener.childClicked(v, getAdapterPosition());
        }
    }

}
