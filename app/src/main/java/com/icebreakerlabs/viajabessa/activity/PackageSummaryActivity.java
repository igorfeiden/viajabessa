package com.icebreakerlabs.viajabessa.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.icebreakerlabs.viajabessa.R;
import com.icebreakerlabs.viajabessa.adapters.ContentActivityGalleryAdapter;
import com.icebreakerlabs.viajabessa.adapters.ContentActivityPaymentOptionsAdapter;
import com.icebreakerlabs.viajabessa.listeners.GenericClickListener;
import com.icebreakerlabs.viajabessa.model.TravelPackage;
import com.squareup.picasso.Picasso;

public class PackageSummaryActivity extends AppCompatActivity implements GenericClickListener {

    // Android objs
    private Toolbar toolbar;

    // Project objs
    private TravelPackage lPackage;
    private TextView description_tv;
    private TextView prices_prefix_tv;
    private RecyclerView description_gallery_rv;
    private RecyclerView prices_rv;
    private ContentActivityPaymentOptionsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_summary);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent() != null && getIntent().getExtras() != null) {
            lPackage = ((TravelPackage) getIntent().getExtras().get(MainActivity.HOME_PACKAGE));
        }

        setupToolbar();
        setupParallax();

        setupViews();

    }

    private void setupToolbar() {
        toolbar.setTitle(lPackage != null ?
                lPackage.getTitle() :
                "Saiba mais");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setupParallax() {

        if (lPackage != null) {
            Picasso.with(this)
                    .load(lPackage.getBigImageURL())
                    .into(((ImageView) findViewById(R.id.summary_header)));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) finish();

        return super.onOptionsItemSelected(item);
    }

    private void setupViews() {
        description_tv = findViewById(R.id.content_description_textview);
        prices_prefix_tv = findViewById(R.id.content_buying_prefix_textview);

        description_gallery_rv = findViewById(R.id.content_gallery_recyclerview);
        prices_rv = findViewById(R.id.content_buying_prices_recyclerview);

        if (lPackage != null) {

            description_tv.setText(lPackage.getPkgDescription());
            prices_prefix_tv.setText(lPackage.getBuyingPrefix());

            description_gallery_rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            description_gallery_rv.setAdapter(new ContentActivityGalleryAdapter(this, lPackage.getGallery()));

            adapter = new ContentActivityPaymentOptionsAdapter(lPackage.getVariations(), this);
            adapter.setupListener(this);

            prices_rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            prices_rv.setAdapter(adapter);

        }
    }

    @Override
    public void childClicked(View view, int position) {
        Toast.makeText(this, adapter.getPriceAt(position).getPerks(), Toast.LENGTH_SHORT).show();
    }
}
