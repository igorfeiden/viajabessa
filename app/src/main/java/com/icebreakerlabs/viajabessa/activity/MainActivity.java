package com.icebreakerlabs.viajabessa.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.icebreakerlabs.viajabessa.R;
import com.icebreakerlabs.viajabessa.adapters.HomeRecyclerViewAdapter;
import com.icebreakerlabs.viajabessa.listeners.GenericClickListener;
import com.icebreakerlabs.viajabessa.model.TravelPackage;
import com.icebreakerlabs.viajabessa.network.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements GenericClickListener {

    public static final String HOME_PACKAGE = "HOME_PCKG";

    // Android objs
    RecyclerView recyclerView;
    ProgressBar progressBar;

    // Project objs
    private HomeRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_name));
        setContentView(R.layout.activity_main);

        initViews();
        startRequests();

    }

    private void initViews() {
        progressBar = findViewById(R.id.home_progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        recyclerView = findViewById(R.id.home_recycler_view);
        recyclerView.setVisibility(View.GONE);
    }

    private void startRequests() {

        RetrofitService service = RetrofitService.ServiceGenerator.createService(RetrofitService.class);
        service.getPackages()
                .enqueue(new Callback<List<TravelPackage>>() {
                    @Override
                    public void onResponse(Call<List<TravelPackage>> call, Response<List<TravelPackage>> response) {

                        reportToFirebase();

                        if (response.isSuccessful()) {
                            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));

                            adapter = new HomeRecyclerViewAdapter(response.body(), MainActivity.this);
                            adapter.setupListener(MainActivity.this);
                            recyclerView.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<TravelPackage>> call, Throwable t) {
                        Log.e(MainActivity.class.getSimpleName(), t.getMessage(), t);
                    }
                });


    }

    /**
     * Since Retrofit keeps a good cache of the informations,
     * I only need to make one single request, to get the travels.
     * So that's why the mectrics are only sent here
     * <p>
     * Also, I couldn't understand how the apiary service would actually record the mectrics.
     * So, I decided to use Firebase service because the reports could be
     * easily exported to an CSV file in a daily basis.
     * With the approximation below, I am sure I can provide updates each 24h period.
     */
    private void reportToFirebase() {

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, Build.MODEL);
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, Build.BRAND);
        bundle.putString(FirebaseAnalytics.Param.ORIGIN, String.valueOf(Build.VERSION.RELEASE));
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

    }


    @Override
    public void childClicked(View view, int position) {
        TravelPackage packageAt = adapter.getPackageAt(position);

        Intent intent = new Intent(MainActivity.this, PackageSummaryActivity.class);
        intent.putExtra(HOME_PACKAGE, packageAt);
        startActivity(intent);
    }
}
