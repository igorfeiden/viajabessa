# README #

A empresa Viajabessa contratou a Bemobi para desenvolver um aplicativo de viagens
onde é possível efetuar compras de pacotes de viagens com preços promocionais.

### Explicação de elementos ###

* Envio de Métricas:
Como solicitado, o empregador deseja informações como versão do Android, número e modelo. Estas informações podem ser obtidas automaticamente ao se utilizar o Firebase.
Criei, porém, uma chamada  para explicitamente obter estes dados para demonstrar como podem ser requisitados via código.

* Imagens:
Como solicitado, o aplicativo mostra uma imagem do destino em parallax no topo de cada um. Porém, decidi acrescentar uma pequena galeria, como um app de viagens incluiria.
Com mais imagens do destino, maiores as chances de que o comprador se interesse por tal.

### Changelog ###

* Versão 0.1

- Versão inicial com reporting via Firebase. (A cada 24h, a URL http://demo6391057.mockable.io/informations será atualizada com os dados do Firebase)*
- Pacotes de viagem via arquivo de Json disponíveis no endpoint: http://demo6391057.mockable.io/packages

### Autor ###
* Igor Feiden
- igorfeiden@hotmail.com

